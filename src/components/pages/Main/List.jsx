import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const ArtistList = styled.ul`
  padding-top: 50px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const Artist = styled.li`
  border: 1px solid rgba(0, 0, 0, .125);
  border-radius: .5rem;
  margin-bottom: 15px;
  background-color: #fff;
  box-shadow: 0 1px 15px rgba(0, 0, 0, .04), 0 1px 6px rgba(0, 0, 0, .04);
  cursor: pointer;
  transition: transform .3s linear;
  margin-left: 10px;
  
  a {
    display: flex;
    padding: 1.75rem;
  }
      
  &:nth-child(even) {
    background-color: #efefef;
  }
  
  &:hover {
    transform: scale(1.05);
  }

  
`;

const ArtistBio = styled.p`
  font-size: 14px;
  color: green;
  text-align: center;
`;

const ArtistTitle = styled.h5`
  text-align: center;
`;

const List = ({ artists }) => (
  <ArtistList>
    {artists.map((item) => (
      <Link to={`artists/${item._id}`}>
        <Artist className="card" key={item._id}>
          <img src={item.imageUrl} className="card-img-top" />
          <div class="card-body">
              <ArtistTitle className="card-title">{item.name}</ArtistTitle>
              <ArtistBio className="card-text">{`(${item.bio})`}</ArtistBio>
          </div>
        </Artist>
      </Link>
    ))}
  </ArtistList>
);

List.propTypes = {
  artists: PropTypes.arrayOf(PropTypes.object),
};

List.defaultProps = {
  artists: [],
};

export default List;